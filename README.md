Master

[![Pipeline status](https://gitlab.com/mbehrle/product_variant/badges/master/pipeline.svg)](https://gitlab.com/mbehrle/product_variant/commits/master)
[![Coverage report](https://gitlab.com/mbehrle/product_variant/badges/master/coverage.svg?job=coverage)](http://mbehrle.gitlab.io/product_variant)

Develop

[![Pipeline status](https://gitlab.com/mbehrle/product_variant/badges/develop/pipeline.svg)](https://gitlab.com/mbehrle/product_variant/commits/master)
[![Coverage report](https://gitlab.com/mbehrle/product_variant/badges/develop/coverage.svg?job=coverage)](http://mbehrle.gitlab.io/product_variant)

Custom

[![coverage report](https://gitlab.com/mbehrle/product_variant/badges/develop/coverage.svg)](https://gitlab.com/mbehrle/product_variant/commits/develop)


Test

This module runs with the Tryton application platform.

Installing
----------

See INSTALL

Note
----

This module is developed and tested over a Tryton server and core modules
patched with [trytond-patches](https://gitlab.com/m9s/trytond-patches).
Maybe some of these patches are required for the module to work.

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/product_variant/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, forum or IRC channel:

   * http://bugs.tryton.org/
   * http://www.tryton.org/forum
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

